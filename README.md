## OAuth 1.0 for Service providers [![Build Status](https://travis-ci.org/ericaro/oauthprovider.png?branch=master)](https://travis-ci.org/ericaro/oauthprovider)

(if you are interested in a Client side version check out [this version](https://github.com/ericaro/oauth) )

[Read the doc](http://godoc.org/github.com/ericaro/oauthprovider)


TODO: 

 - full test of the solution
 - add some configurations
